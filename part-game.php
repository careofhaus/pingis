<?php
$game = $_GET["game"];

if($game == "all"){
    $sql = "SELECT * FROM scores WHERE confirmed=1 ORDER BY id DESC";
} else {
    $sql = "SELECT * FROM scores WHERE id='$game' AND confirmed=1";
}

$result = $conn->query($sql);
?>

<div class="board game <?= $game == "all" ? "list-all":"single" ?>" id="games">
    <?php if($game == "all"){ ?>
    <h1>Alla matcher (<?= $result->num_rows ?>st)</h1>
    <?php } ?>
    <ul>
        <?php
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {

                $set3 = !empty($row["set3"]) ? $row["set3"] : false;

                // get som extra user data depending on who's who
                $pl1 = $row["player1"];
                $sql = "SELECT * FROM users WHERE slack_user_name = '$pl1'";
                $res = $conn->query($sql);
                $pl1 = (object) $res->fetch_assoc();

                $pl2 = $row["player2"];
                $sql = "SELECT * FROM users WHERE slack_user_name = '$pl2'";
                $res = $conn->query($sql);
                $pl2 = (object) $res->fetch_assoc();

                $i=0;
                ?>

                <?php if($game != "all") { ?>
                    <div class="card board rank-<?= $i ?> <?= $row["winner"] == $pl1->slack_user_name ? 'winner':'looser' ?>" style="background-image:url('<?= $pl1->avatar ?>')">
                        <a href="/?user=<?= $pl1->slack_user_name ?>">
                            <div class="tint"></div>
                            <div class="info">
                                <h1 class="name"><?= empty($pl1->nick) ? $pl1->fname : $pl1->nick ?></h1>
                                <div class="bottom">
                                    <span class="elo"><img class="icon" src="/assets/img/elo.svg"><?= $pl1->elo ?></span>
                                    <span class="wins"><img class="icon" src="/assets/img/win.svg"><?= $pl1->wins ?></span>
                                    <span class="loss"><img class="icon" src="/assets/img/loss.svg"><?= $pl1->loss ?></span>
                                </div>
                            </div>
                        </a>
                    </div>
                <?php } ?>

                <li>
                <?php if($game == "all") { ?>
                    <div class="avatar"><img src="<?= $pl1->avatar ?>"/></div>
                    <?php } ?>
                    <div class="name"><?= $pl1->fname ?></div>
                    <div class="score">
                        <div class="set">
                            <span class="elo <?= $row["elo_pl1_set1"] >= 0 ? 'plus' : 'minus'; ?>"><?= $row["elo_pl1_set1"] >= 0 ? '+' : ''; ?><?= $row["elo_pl1_set1"] ?></span>
                            <span class="res"><?= $row["set1"] ?></span>
                            <span class="elo right <?= $row["elo_pl2_set1"] >= 0 ? 'plus' : 'minus'; ?>"><?= $row["elo_pl2_set1"] >= 0 ? '+' : ''; ?><?= $row["elo_pl2_set1"] ?></span>
                        </div>
                        <div class="set">
                            <span class="elo <?= $row["elo_pl1_set2"] >= 0 ? 'plus' : 'minus'; ?>"><?= $row["elo_pl1_set2"] >= 0 ? '+' : ''; ?><?= $row["elo_pl1_set2"] ?></span>
                            <span class="res"><?= $row["set2"] ?></span>
                            <span class="elo right <?= $row["elo_pl2_set2"] >= 0 ? 'plus' : 'minus'; ?>"><?= $row["elo_pl2_set2"] >= 0 ? '+' : ''; ?><?= $row["elo_pl2_set2"] ?></span>
                        </div>
                        <?php if ($set3) { ?>
                            <div class="set">
                                <span class="elo <?= $row["elo_pl1_set3"] >= 0 ? 'plus' : 'minus'; ?>"><?= $row["elo_pl1_set3"] >= 0 ? '+' : ''; ?><?= $row["elo_pl1_set3"] ?></span>
                                <span class="res"><?= $row["set3"] ?></span>
                                <span class="elo right <?= $row["elo_pl2_set3"] >= 0 ? 'plus' : 'minus'; ?>"><?= $row["elo_pl2_set3"] >= 0 ? '+' : ''; ?><?= $row["elo_pl2_set3"] ?></span>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="name right"><?= $pl2->fname ?></div>
                    <?php if($game == "all") { ?>
                        <div class="avatar"><img src="<?= $pl2->avatar ?>"/></div>
                    <?php } ?>

                </li>
                <?php if($game != "all") { ?>
                    <div class="card board rank-<?= $i ?> <?= $row["winner"] == $pl2->slack_user_name ? 'winner':'looser' ?>" style="background-image:url('<?= $pl2->avatar ?>')">
                        <a href="/?user=<?= $pl2->slack_user_name ?>">
                            <div class="tint"></div>
                            <div class="info">
                                <h1 class="name"><?= empty($pl2->nick) ? $pl2->fname : $pl2->nick ?></h1>
                                <div class="bottom">
                                    <span class="elo"><img class="icon" src="/assets/img/elo.svg"><?= $pl2->elo ?></span>
                                    <span class="wins"><img class="icon" src="/assets/img/win.svg"><?= $pl2->wins ?></span>
                                    <span class="loss"><img class="icon" src="/assets/img/loss.svg"><?= $pl2->loss ?></span>
                                </div>
                            </div>
                        </a>
                    </div>
                <?php }
            }
        } ?>
    </ul>
</div>

<?php if($game != "all"){
    include "part-leaderboard.php";
}
