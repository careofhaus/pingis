<div class="board leaderboard">
    <?php
    $sql = "SELECT * FROM users ORDER BY elo DESC";
    $result = $conn->query($sql);
    ?>
    <ul>

        <?php
        $not_qualified = [];

        if ($result->num_rows > 0) {
            $i=0;



            while ($row = $result->fetch_assoc()) {
                $row = (object) $row;
                if($row->wins+$row->loss < 5){
                    $fname = $row->fname;
                    $user = $row->slack_user_name;
                    $games = $row->wins+$row->loss;
                    $arr = ["fname"=>$fname, "user"=>$user, "games"=>$games];
                    array_push($not_qualified, $arr);
                    continue;
                }
                $i++;

                if($i < 4){ ?>
                    <div class="card board rank-<?= $i ?>" style="background-image:url('<?= $row->avatar ?>')">
                        <a href="/?user=<?= $row->slack_user_name ?>">
                            <div class="rank"><?= $i ?></div>
                            <div class="tint"></div>
                            <div class="info">
                                <h1 class="name"><?= empty($row->nick) ? $row->fname : $row->nick ?></h1>
                                <div class="bottom">
                                    <span class="elo"><img class="icon" src="/assets/img/elo.svg"><?= $row->elo ?></span>
                                    <span class="wins"><img class="icon" src="/assets/img/win.svg"><?= $row->wins ?></span>
                                    <span class="loss"><img class="icon" src="/assets/img/loss.svg"><?= $row->loss ?></span>
                                </div>
                            </div>
                        </a>

                    </div>

                <?php }
                if($i == 3){ ?>
                    <li class="top">
                        <div class="rank"></div>
                        <div class="avatar"></div>
                        <div class="name"></div>
                        <div class="wins">Vinster</div>
                        <div class="loss">Förluster</div>
                        <div class="elo">Poäng</div>
                    </li>
                <?php } ?>
                <?php if($i > 3){ ?>
                    <li>
                        <a class="row" href="/?user=<?= $row->slack_user_name ?>">
                            <div class="rank"><?= $i ?></div>
                            <div class="avatar"><img src="<?= $row->avatar ?>"/></div>
                            <div class="name"><?= empty($row->nick) ? $row->fname : $row->nick ?></div>
                            <div class="wins"><?= $row->wins ?></div>
                            <div class="loss"><?= $row->loss ?></div>
                            <div class="elo"><?= $row->elo ?></div>
                        </a>
                    </li>

            <?php }
            }
        } ?>
    </ul>
    <p class="qualify">Man måste spela minst 5 matcher för att komma med på leaderboarden. De som inte än kvalat in är:
        <br/>
    <?php
    foreach ($not_qualified as $user){
        echo "<a href='/?user=".$user['user']."'>".$user['fname']." (". $user['games'] .")</a> ";
    }
    ?>
    </p>
</div>