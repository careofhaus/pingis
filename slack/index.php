<?php

/*
 * Connect to DB
 */
$servername = "127.0.0.1";
$username = "pingis";
$password = "nTlO4o1sjs0p";
$dbname = "pingis";

if($_SERVER['HTTP_HOST'] == "pingis.test"){
    $servername = "localhost";
    $username = "haus";
    $password = "life";
    $dbname = "pingis";
}

$conn = new mysqli($servername, $username, $password, $dbname);



if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

/*
 * Extract Slack info
 */

// check envirnoment
if ($_SERVER['HTTP_HOST'] == "pingis.test") {
    $slack = (object) [
        "token" => "token-schmoken",
        "team_id" => "T0EQC1THN",
        "team_domain" => "wearehaus",
        "channel_id" => "CDANCHNUC",
        "channel_name" => "pingis-api",
        //"user_id" => "U21NQ4FHA", // Simon
        //"user_id" => "U05A4SSKUTF", // Alex
        //"user_id" => "U8SM6R7J4", // Jonatan
        "user_id" => "U02AGPA9TPF", // Elmer
        //"user_id" => "U0FSX3A90", // Bjerneholt
        "user_name" => "elmer",
        "command" => "/pingis",
        "text" => $_GET["text"],
        "response_url" => "response-url",
        "trigger_id" => "trigger-id"
    ];
}
else {
    // get real data from slack post
    $slack = (object) $_POST;
}

$string_parts = explode(" ", $slack->text);
$command = $string_parts[0];

// check for result input
if ((strpos($command, '@') !== false) && (strpos($slack->text, ':') !== false)) {
    $command = "resultat";
}

/*
 * Check what to do
 */

// check auto confirm scores first
auto_confirm();

switch ($command) {
    case 'hej' :
        welcome();
        break;
    case 'resultat' :
        insert_result(false);
        break;
    case 'ok' :
        insert_result(true);
        break;
    case 'nej' :
        insert_result(true);
        break;
    case 'hjälp' :
        help();
        break;
    case 'init' :
        //init_app();
        break;
}

//show_input(); // for dev

/*
 * /.. hej
 */

function welcome()
{
    global $conn, $slack;

    $output = '';
    $sql = "SELECT id FROM users WHERE slack_user_id = '$slack->user_id'";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        // user exists
        echo 'Hej kompis, du kan checka hur det går här -> http://pingis.haus.se';
    }
    else {
        // Get some data with api and create user
        $token = "xoxp-14828061600-69772151588-523053574562-c44af1555dda01671dd445d42ca79863";
        $url = "https://slack.com/api/users.profile.get?token=$token&user=$slack->user_id&pretty=1";
        $result = json_decode(getCurl($url));

        $user_first_name = $result->profile->first_name;
        $user_last_name = $result->profile->last_name;
        $avatar = $result->profile->image_512;

        $sql = "INSERT INTO users (slack_user_id, slack_user_name, fname, lname, avatar, wins, loss, plus_points, minus_points, elo) VALUES ('$slack->user_id', '$slack->user_name', '$user_first_name', '$user_last_name', '$avatar', 0, 0, 0, 0, 1000)";
        if ($conn->query($sql) === true) {
            echo "Välkommen $user_first_name, nu kör vi!\n\nFör att komma igång är det bara att spela en match mot en kompis och sen registrera resultatet här. Matcher spelas till 11, bäst av tre set. Din motståndare behöver sen godkänna resultatet för att det ska sparas.\n\nDu kan sedan följa hur det går på http://pingis.haus.se\n\n";
            help();
            bot("BOOM. Välkommen $user_first_name! :raised_hands:");
        }
        else {
            echo "\n```$conn->error```\n";
        }
    }
}

/*
 * /.. @motståndare
 * /.. ok
 */

function insert_result($confirmation, $auto_row = false)
{
    global $conn, $slack, $string_parts, $command;

    $output = '';

    /*
    * resultat @jim 10:21 11:21 21:19
    */

    // if reporting
    if (!$confirmation) {

        // check input with regex (https://regex101.com/r/pQ1ytw/2)
        if(!preg_match("/[@]+\S+\s+(\d+)[:](\d+)\s(\d+)[:](\d+)(\s(\d+)[:](\d+))?+$/", $slack->text)){
            die("Kontrollera stavning..  Så här skriver man: `@motståndare 11:6 6:11 11:3` (Dina poäng först).");
        }

        $user1 = $slack->user_name;
        $arr = explode('@', $string_parts[0]);
        $user2 = $arr[1];

        // check input with regex
        /*
        if (1==1) {
            die("Oh oh! De där resultatet ser inte bra ut, skriv in så här: ```/pingis resultat @motståndare 11:7 5:11 11:8```");
        }
        */

        // check result numbers depending on report/confirm
        $set1 = !empty($string_parts[1]) ? $string_parts[1] : '';
        $set2 = !empty($string_parts[2]) ? $string_parts[2] : '';
        $set3 = !empty($string_parts[3]) ? $string_parts[3] : false;

        // check numbers and build numbers object
        $scores = check_score($set1, $set2, $set3, $user1, $user2);
        if ($scores->error) {
            die($scores->error);
        }

        // verify players
        $sql = "SELECT id FROM users WHERE slack_user_name = '$user1' OR slack_user_name = '$user2'";
        $result = $conn->query($sql);
        if ($result->num_rows < 2) {
            die('Någon av spelarna är inte registrerade än.. Innan du loggar resultat ska båda spelarna skrivit: ```/pingis hej```');
        }

        $sql = "INSERT INTO scores (player1, player2, set1, set2, set3, winner, time, season, confirmed)
      VALUES ('$user1', '$user2', '$set1', '$set2', '$set3', '$scores->winner', now(), 1, 0)";
        if ($conn->query($sql) === true) {
            $user1_result = $user1 == $scores->winner ? "vann" : "förlorade";
            $set3_res = $set3 ? "/ $set3" : "";
            bot("*$user1* $user1_result mot *$user2* ($set1 / $set2 $set3_res)\n@$user2, för att bekräfta skriv: `/pingis ok @$user1` Eller: `/pingis nej @$user1` _(Autobekräftas efter 24h)_");
        }
        else {
            echo "\n```$conn->error```\n";
        }
    }
    else {
        // the confirmer is always player2 in DB
        $user2 = $slack->user_name;
        $arr = explode('@', $string_parts[1]);
        $user1 = $arr[1];

        // if auto confirm
        if ($auto_row) {
            $user1 = $auto_row->player1;
            $user2 = $auto_row->player2;
        }

        // Include ELO calculation class
        require_once '../assets/elo-rating-master/src/Rating/Rating.php';

        // get users ELO
        $user1_elo = $user2_elo = 0;
        $sql = "SELECT slack_user_name,elo FROM users WHERE slack_user_name = '$user1' OR slack_user_name = '$user2'";
        $result = $conn->query($sql);
        while ($row = $result->fetch_assoc()) {
            if ($row["slack_user_name"] == $user1) {
                $user1_elo = $row["elo"];
            }
            if ($row["slack_user_name"] == $user2) {
                $user2_elo = $row["elo"];
            }
        }

        // Confirm correct result
        $sql = "SELECT * FROM scores WHERE confirmed = 0";

        if ($auto_row) {
            $sql = "SELECT * FROM scores WHERE id = $auto_row->id";
        }

        $result = $conn->query($sql);

        while ($res = $result->fetch_assoc()) {
            $row = (object) $res;
            // loop and find first score set that has both players
            // player1 is always the reporter and player2 is always the confirmer in DB
            if ($row->player1 == $user1 && $row->player2 == $user2) {

                // Check for set 3
                $set3 = $row->set3 === NULL ? false : $row->set3;

                // check numbers and build numbers object
                $scores = check_score($row->set1, $row->set2, $set3, $user1, $user2);

                // set 1
                if ($scores->score_user1_set_1 > $scores->score_user2_set_1) {
                    $rating = new Rating($user1_elo, $user2_elo, Rating::WIN, Rating::LOST);
                }
                else {
                    $rating = new Rating($user1_elo, $user2_elo, Rating::LOST, Rating::WIN);
                }
                $results = $rating->getNewRatings();
                //Calculate new elo after each set
                $elo_pl1_set1 = ceil((int) $results["a"] - (int) $user1_elo);
                $elo_pl2_set1 = ceil((int) $results["b"] - (int) $user2_elo);

                // set 2
                if ($scores->score_user1_set_2 > $scores->score_user2_set_2) {
                    // input elo is from set 1 difference
                    $rating = new Rating(($user1_elo + $elo_pl1_set1), ($user2_elo + $elo_pl2_set1), Rating::WIN, Rating::LOST);
                }
                else {
                    $rating = new Rating(($user1_elo + $elo_pl1_set1), ($user2_elo + $elo_pl2_set1), Rating::LOST, Rating::WIN);
                }
                $results = $rating->getNewRatings();
                $elo_pl1_set2 = ceil((int) $results["a"] - ($user1_elo + $elo_pl1_set1));
                $elo_pl2_set2 = ceil((int) $results["b"] - ($user2_elo + $elo_pl2_set1));

                $elo_pl1_set3 = 0;
                $elo_pl2_set3 = 0;

                // set 3
                if ($set3) {
                    if ($scores->score_user1_set_3 > $scores->score_user2_set_3) {
                        $rating = new Rating(($user1_elo + $elo_pl1_set1 + $elo_pl1_set2), ($user2_elo + $elo_pl2_set1 + $elo_pl2_set2), Rating::WIN, Rating::LOST);
                    }
                    else {
                        $rating = new Rating(($user1_elo + $elo_pl1_set1 + $elo_pl1_set2), ($user2_elo + $elo_pl2_set1 + $elo_pl2_set2), Rating::LOST, Rating::WIN);
                    }
                    $results = $rating->getNewRatings();
                    $elo_pl1_set3 = ceil((int) $results["a"] - ($user1_elo + $elo_pl1_set1 + $elo_pl1_set2));
                    $elo_pl2_set3 = ceil((int) $results["b"] - ($user2_elo + $elo_pl2_set1 + $elo_pl2_set2));

                    // new Elo after game ended
                    $elo_pl1_final = ($user1_elo + $elo_pl1_set1 + $elo_pl1_set2 + $elo_pl1_set3);
                    $elo_pl2_final = ($user2_elo + $elo_pl2_set1 + $elo_pl2_set2 + $elo_pl2_set3);
                }
                else {
                    $elo_pl1_final = ($user1_elo + $elo_pl1_set1 + $elo_pl1_set2);
                    $elo_pl2_final = ($user2_elo + $elo_pl2_set1 + $elo_pl2_set2);
                }

                // if deny
                if ($command == "nej") {
                    $sql = "UPDATE scores SET confirmed=-1 WHERE id=$row->id";
                    if ($conn->query($sql) === true) {
                        bot(":skull: *$user2* förnekade sin match mot *$user1*");
                    }
                    die();
                }

                // double check ELO calculations so winner wont loose ELO score
                if (($scores->winner == $user1) && ($elo_pl1_set1 + $elo_pl1_set2 + $elo_pl1_set3 < 0)) {
                    if ($elo_pl1_set1 < 0) {
                        $elo_pl1_set1 = (($elo_pl1_set2 + $elo_pl1_set3) * -1) + 1; // always go 1 plus for win
                    }
                    if ($elo_pl1_set2 < 0) {
                        $elo_pl1_set2 = (($elo_pl1_set1 + $elo_pl1_set3) * -1) + 1;
                    }
                    $elo_pl1_final = ($user1_elo + $elo_pl1_set1 + $elo_pl1_set2 + $elo_pl1_set3);
                }
                else if (($scores->winner == $user2) && ($elo_pl2_set1 + $elo_pl2_set2 + $elo_pl2_set3 < 0)) {
                    if ($elo_pl2_set1 < 0) {
                        $elo_pl2_set1 = (($elo_pl2_set2 + $elo_pl2_set3) * -1) + 1;
                    }
                    if ($elo_pl2_set2 < 0) {
                        $elo_pl2_set2 = (($elo_pl2_set1 + $elo_pl2_set3) * -1) + 1;
                    }
                    $elo_pl2_final = ($user2_elo + $elo_pl2_set1 + $elo_pl2_set2 + $elo_pl2_set3);
                }

                $sql = "UPDATE scores SET confirmed=1, elo_pl1_start='$user1_elo', elo_pl2_start='$user2_elo', elo_pl1_set1='$elo_pl1_set1', elo_pl2_set1='$elo_pl2_set1', elo_pl1_set2='$elo_pl1_set2', elo_pl2_set2='$elo_pl2_set2', elo_pl1_set3='$elo_pl1_set3', elo_pl2_set3='$elo_pl2_set3' WHERE id=$row->id";
                if ($conn->query($sql) === true) {
                    //update user main elo

                    $stat = ($scores->winner == $user2 ? 'wins' : 'loss');
                    $sql = "UPDATE users SET elo=$elo_pl2_final, $stat=$stat+1 WHERE slack_user_name='$user2'";
                    $conn->query($sql);

                    $stat = ($scores->winner == $user1 ? 'wins' : 'loss');
                    $sql = "UPDATE users SET elo=$elo_pl1_final, $stat=$stat+1 WHERE slack_user_name='$user1'";
                    $conn->query($sql);

                    $user1_result = $user1 == $scores->winner ? "vinst" : "förlust";
                    $user2_result = $user2 == $scores->winner ? "vinst" : "förlust";

                    $set3_res = $set3 ? "/ $set3" : "";

                    // throw different message depending on auto confirm or not
                    if ($auto_row) {
                        bot(":robot_face: Automatisk bekräftelse:\n*$user1* ($user1_result) mot *$user2* ($row->set1 / $row->set2 $set3_res)");
                    }
                    else {
                        bot(":punch: *$user2* bekräftade sin $user2_result mot *$user1*\nhttp://pingis.haus.se/?game=$row->id");
                        if ($scores->cake) {
                            $cake_gif = get_gif("cake");
                            bot("Damn @" . $scores->cake . " ... *11:0*..?\n$cake_gif");
                        }
                    }

                    $output = "";
                    break; // just confirm one
                }
                else {
                    echo "\n```$conn->error```\n";
                }
            }
            else {
                $output = "Eeeh.. inga matcher att bekräfta!";
            }
        }
    }

    // user stats is updated on confirmation

    echo $output;
}

/*
 * Helper for checking results
 */

function check_score($set1, $set2, $set3, $user1, $user2)
{

    $scores = [];
    $scores['error'] = false;
    $scores['cake'] = false;
    $user1_total_sets = 0;
    $user2_total_sets = 0;

    // error messages
    $error1 = ":see_no_evil: Oh oh! Man måste spela till minst 11!";
    $error2 = ":see_no_evil: Oh oh! Man måste vinna med 2 poäng!";
    $error3 = ":see_no_evil: Oh oh! Det får inte bli oavgjort!";
    $error4 = ":see_no_evil: Oh oh! Endast ett set räcker inte, bäst av tre gäller!";
    $error5 = ":see_no_evil: Oh oh! Du kan inte regga matcher mot dig själv!";

    if ($user1 == $user2) {
        die($error5);
    }

    // separate values
    $set1_arr = explode(':', $set1);
    $set2_arr = explode(':', $set2);

    // check for set count
    if (!$set2) {
        die($error4);
    }

    $scores['score_user1_set_1'] = (int) $set1_arr[0];
    $scores['score_user2_set_1'] = (int) $set1_arr[1];
    $scores['score_user1_set_2'] = (int) $set2_arr[0];
    $scores['score_user2_set_2'] = (int) $set2_arr[1];

    // check sets
    // set 1
    if ($scores['score_user1_set_1'] > $scores['score_user2_set_1']) {
        $user1_total_sets++;
        $winner_score_set1 = $scores['score_user1_set_1'];
        $looser_score_set1 = $scores['score_user2_set_1'];
    }
    else {
        $user2_total_sets++;
        $winner_score_set1 = $scores['score_user2_set_1'];
        $looser_score_set1 = $scores['score_user1_set_1'];
    }

    //set 2
    if ($scores['score_user1_set_2'] > $scores['score_user2_set_2']) {
        $user1_total_sets++;
        $winner_score_set2 = $scores['score_user1_set_2'];
        $looser_score_set2 = $scores['score_user2_set_2'];
    }
    else {
        $user2_total_sets++;
        $winner_score_set2 = $scores['score_user2_set_2'];
        $looser_score_set2 = $scores['score_user1_set_2'];
    }

    // control numbers and throw errors
    if ($winner_score_set1 < 11) {
        die($error1);
    }
    if ($winner_score_set1 < $looser_score_set1 + 2) {
        die($error2);
    }
    if ($winner_score_set2 < $looser_score_set2 + 2) {
        die($error2);
    }

    // check for cake
    if (($scores['score_user1_set_1'] == 0) || ($scores['score_user1_set_2'] == 0)) {
        $scores['cake'] = $user1;
    }
    else if (($scores['score_user2_set_1'] == 0) || ($scores['score_user2_set_2'] == 0)) {
        $scores['cake'] = $user2;
    }

    // If 3 sets
    if ($set3) {
        $set3_arr = explode(':', $set3);
        $scores['score_user1_set_3'] = (int) $set3_arr[0];
        $scores['score_user2_set_3'] = (int) $set3_arr[1];

        if ($scores['score_user1_set_3'] > $scores['score_user2_set_3']) {
            $user1_total_sets++;
            $winner_score_set3 = $scores['score_user1_set_3'];
            $looser_score_set3 = $scores['score_user2_set_3'];
        }
        else {
            $user2_total_sets++;
            $winner_score_set3 = $scores['score_user2_set_3'];
            $looser_score_set3 = $scores['score_user1_set_3'];
        }

        // control numbers and throw errors
        if ($winner_score_set3 < 11) {
            die($error1);
        }
        if ($winner_score_set3 < $looser_score_set3 + 2) {
            die($error2);
        }

        // check for cake
        if ($scores['score_user1_set_3'] == 0) {
            $scores['cake'] = $user1;
        }
        else if ($scores['score_user2_set_3'] == 0) {
            $scores['cake'] = $user2;
        }
    }

    // check for draw
    if ($user1_total_sets == $user2_total_sets) {
        die($error3);
    }

    // winner
    $scores['winner'] = $user1_total_sets > $user2_total_sets ? $user1 : $user2;
    $scores['loser'] = $user1_total_sets < $user2_total_sets ? $user1 : $user2;

    return (object) $scores;
}

/*
 * /help
 */

function help()
{
    $output = "*Registrera en match* ```/pingis @motståndare 11:3 5:11 11:9``` \n";
    $output .= "*Godkänn resultat* ```/pingis ok @rapporterare``` \n";
    $output .= "*Registrera dig som användare* ```/pingis hej``` \n";
    $output .= "*Visa hjälp* ```/pingis hjälp``` ";
    echo $output;
}

/*
 * /init
 */

function init_app()
{
    $output = "*Pingisligan är nu nollställd och redo för nästa omgång!*\n\n";
    $output .= "För att vara med, skriv: `/pingis hej` och följ #pingpong -kanalen. :fist:";
    $output .= "";
    bot($output);
}

/*
 * Autoconfirm scores
 */

function auto_confirm()
{

    global $conn;
    $sql = "SELECT * FROM scores WHERE confirmed=0 AND time < NOW() - INTERVAL 1 DAY";
    $result = $conn->query($sql);
    while ($res = $result->fetch_assoc()) {
        $row = (object) $res;
        insert_result(true, $row);
    }
}

/*
 * Slack bot
 */

function bot($str)
{

    $token = "xoxb-14828061600-546322117670-rqdbGKRdk4N5yGfYKRJlrScG";
    // CHANGE HERE WHEN DEVVING!
    $channel = "CDANCHNUC"; // #pingis-api
    //$channel = "CG7AKT3UM"; // #pingpong
    //$channel = "C0EQAPD6D"; // #hela-hauset
    $str = urlencode($str);

    $headers[] = "Content-Type: application/json";
    $url = "https://slack.com/api/chat.postMessage?token=" . $token . "&channel=" . $channel . "&text=" . $str . "&pretty=1&link_names=true";

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_exec($ch);
    curl_close($ch);
}

/*
 * Giphy
 */
function get_gif($tag)
{

    $url = 'http://api.giphy.com/v1/gifs/search?api_key=fvLd9q2d1FAu8qrdwPWbufwu6oVmRaF9&rating=pg-13&limit=50&q=' . $tag;
    $result = getCurl($url);

    $json = json_decode($result);

    $count = count($json->data);
    $rand = rand(0, $count - 1);

    $url = $json->data[$rand]->images->downsized->url;

    return $url;
}

/*
 * var dump
 */
function show_input()
{
    global $slack;

    $output = " ---- SLACK raw input ----\n\n";
    foreach ($slack as $key => $value) {
        $output .= "$key -> $value\n";
    }

    echo ' ```' . $output . '``` ';
}

/*
 * Curl helper
 */
function getCurl($url)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    $result = curl_exec($ch);
    curl_close($ch);

    return $result;
}

?>
