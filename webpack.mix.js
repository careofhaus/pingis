// Docs ->  https://github.com/laravel-mix/laravel-mix

let mix = require('laravel-mix');

mix.sass('./assets/styles/style.scss', './dist').sourceMaps();

mix.js([
    './assets/js/script.js',
], './dist').vue();
