<?php

include "slack-oauth.php";

$servername = "127.0.0.1";
$username = "haus";
$password = "life";
$dbname = "pingis";

if($_SERVER['HTTP_HOST'] == "pingis.test"){
    $servername = "localhost";
    $username = "haus";
    $password = "life";
    $dbname = "pingis";
}
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Pingis</title>
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico">
    <link rel="stylesheet" href="assets/styles/css/normalize.css" media="all">
    <link rel="stylesheet" href="dist/style.css?<?= rand() ?>" media="all">
</head>
<body class="home">
<div class="header">
    <a class="logo" href="/"><img src="assets/img/pingpong.svg"/></a>
    <ul class="menu">
        <li><a href="/">Leaderboard</a></li>
        <li><a href="/?game=all">Matcher</a></li>
        <!--
        <li>
            <a href="https://slack.com/oauth/authorize?scope=identity.basic,identity.avatar&client_id=14828061600.522916202771">
                Logga in (Slack)
            </a>
        </li>
        -->
    </ul>
</div>

<div class="main">

    <?php
    if (isset($_GET["game"])) {
        include "part-game.php";
    }
    else if (isset($_GET["user"])) {
        include "part-user.php";
    }
    else {
        include "part-leaderboard.php";
    } ?>

</div>


<div class="footer">
</div>

</body>
</html>
