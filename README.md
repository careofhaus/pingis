# Pingis

Slack app för Haus pingis

Slack app:
https://api.slack.com/apps/AFCSY5YNP

Hostad på pingis.haus.se

- All Slack funktionalitet och inmatningslogik ligger i slack/index.php

#Dev
- Sätt lokal adress till pingis.test
- Går då att devva lokalt med slash kommandon genom att använda "pingis.test/slack/?text=hej" osv.

#npm
 > npm install
 
 >gulp sass:watch
