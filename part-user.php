<?php
$username = $_GET["user"];
$sql = "SELECT * FROM users WHERE slack_user_name = '$username'";
$result = $conn->query($sql);
$user = (object) $result->fetch_assoc();
?>

<div class="board games" id="games">
    <?php
    $sql = "SELECT * FROM scores WHERE (player1 = '$username' OR player2 = '$username') AND confirmed=1 ORDER BY id DESC";
    $result = $conn->query($sql);
    ?>

    <div class="card" style="background-image:url('<?= $user->avatar ?>')">
        <div class="tint"></div>
        <div class="info">
            <h1 class="name"><?= $user->fname ?></h1>
            <div class="bottom">
                <span class="elo"><img class="icon" src="/assets/img/elo.svg"><?= $user->elo ?></span>
                <span class="wins"><img class="icon" src="/assets/img/win.svg"><?= $user->wins ?></span>
                <span class="loss"><img class="icon" src="/assets/img/loss.svg"><?= $user->loss ?></span>
            </div>
        </div>

    </div>

    <ul>
        <?php
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {

                // get som extra user data depending on who's who
                if ($row["player1"] != $username) {
                    $pl1 = $row["player1"];
                    $sql = "SELECT fname,avatar FROM users WHERE slack_user_name = '$pl1'";
                    $res = $conn->query($sql);
                    $pl1 = (object) $res->fetch_assoc();
                    $pl2 = $user;
                }
                else {
                    $pl2 = $row["player2"];
                    $sql = "SELECT fname,avatar FROM users WHERE slack_user_name = '$pl2'";
                    $res = $conn->query($sql);
                    $pl2 = (object) $res->fetch_assoc();
                    $pl1 = $user;
                }

                $set3 = !empty($row["set3"]) ? $row["set3"] : false;
                ?>
                <li class="<?= $row["winner"] == $username ? 'win' : 'loss'; ?>">
                    <div class="avatar"><img src="<?= $pl1->avatar ?>"/></div>
                    <div class="name"><?= $pl1->fname ?></div>
                    <div class="score">
                        <div class="set">
                            <span class="elo <?= $row["elo_pl1_set1"] >= 0 ? 'plus' : 'minus'; ?>"><?= $row["elo_pl1_set1"] >= 0 ? '+' : ''; ?><?= $row["elo_pl1_set1"] ?></span>
                            <span class="res"><?= $row["set1"] ?></span>
                            <span class="elo right <?= $row["elo_pl2_set1"] >= 0 ? 'plus' : 'minus'; ?>"><?= $row["elo_pl2_set1"] >= 0 ? '+' : ''; ?><?= $row["elo_pl2_set1"] ?></span>
                        </div>
                        <div class="set">
                            <span class="elo <?= $row["elo_pl1_set2"] >= 0 ? 'plus' : 'minus'; ?>"><?= $row["elo_pl1_set2"] >= 0 ? '+' : ''; ?><?= $row["elo_pl1_set2"] ?></span>
                            <span class="res"><?= $row["set2"] ?></span>
                            <span class="elo right <?= $row["elo_pl2_set2"] >= 0 ? 'plus' : 'minus'; ?>"><?= $row["elo_pl2_set2"] >= 0 ? '+' : ''; ?><?= $row["elo_pl2_set2"] ?></span>
                        </div>
                        <?php if ($set3) { ?>
                            <div class="set">
                                <span class="elo <?= $row["elo_pl1_set3"] >= 0 ? 'plus' : 'minus'; ?>"><?= $row["elo_pl1_set3"] >= 0 ? '+' : ''; ?><?= $row["elo_pl1_set3"] ?></span>
                                <span class="res"><?= $row["set3"] ?></span>
                                <span class="elo right <?= $row["elo_pl2_set3"] >= 0 ? 'plus' : 'minus'; ?>"><?= $row["elo_pl2_set3"] >= 0 ? '+' : ''; ?><?= $row["elo_pl2_set3"] ?></span>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="name right"><?= $pl2->fname ?></div>
                    <div class="avatar right"><img src="<?= $pl2->avatar ?>"/></div>
                </li>
            <?php }
        } ?>
    </ul>
</div>